from uuid import uuid4


def image_uploader(instance, filename):
    ext = filename.split('.')[-1]
    new_file_name = f'{uuid4()}.{ext}'
    return f'{instance.__class__.__name__.lower()}/{instance.id}/{new_file_name}'
