from pathlib import Path
import environs

# Build paths inside the project like this: BASE_DIR / 'subdir'.
import django.core.mail.backends.smtp

BASE_DIR = Path(__file__).resolve().parent.parent

env = environs.Env()
env.read_env()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str("DJANGO_SECRET_KEY", "vj7vzaj7mi=q5r97qhx*bt5()1x)(pqdyviwby$g+#y4@#8t72")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool("DJANGO_DEBUG", True)

ALLOWED_HOSTS = ['127.0.0.1']


# Application definition

INSTALLED_APPS = [
    # core apps
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_filters",
    'debug_toolbar',
    # 3d party apps

    # custom apps
    'goods',
    'users',
    'cart',
    'purchases',
    'reviews',
    'core_settings'
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware"
]

ROOT_URLCONF = "_project.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / 'templates']
        ,
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "goods.context_processors.categories",
                "cart.context_processor.cart",
                "goods.context_processors.search_filter",
            ],
        },
    },
]

WSGI_APPLICATION = "_project.wsgi.application"


# Database
# https://docs.djangoproject.com/en/4.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}


# Password validation
# https://docs.djangoproject.com/en/4.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/4.1/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.1/howto/static-files/

STATIC_URL = "static/"

if DEBUG:
    STATICFILES_DIRS = [BASE_DIR / "static/"]
else:
    STATIC_ROOT = BASE_DIR / "static/"

MEDIA_ROOT = BASE_DIR / "media/"
MEDIA_URL = "/media/"

# Max upload filesize
MAX_UPLOAD_SIZE = 10 * 1024 * 1024  # 10MB

# Default primary key field type
# https://docs.djangoproject.com/en/4.1/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# Login redirect
LOGIN_REDIRECT_URL = 'account'

# Auth model
AUTH_USER_MODEL = "users.Account"

# Session cookie
SESSION_COOKIE_AGE = 30 * 24 * 60 * 60

INTERNAL_IPS = [
    '127.0.0.1'
]

#  Only dev email configuration
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

#  SMTP  production configuration
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mail.ru'
EMAIL_PORT = 465
EMAIL_USE_SSL = True
EMAIL_HOST_USER = 'tancock1@mail.ru'
EMAIL_HOST_PASSWORD = 'PQBRvWj2ZLkt9NMvSzk7'
DEFAULT_FROM_EMAIL = 'tancock1@mail.ru'

#  Cart settings
CART_SESSION_ID = 'cart'

# Cash settings
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}


# Redis related settings
REDIS_HOST = env.str('REDIS_PORT')
REDIS_PORT = env.str('REDIS_HOST')

# Celery env's
CELERY_BROKER_URL = 'redis://' + env.str('REDIS_HOST') + ':' + env.str('REDIS_PORT') + '/0'
CELERY_BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600}
CELERY_RESULT_BACKEND = 'redis://' + env.str('REDIS_HOST') + ':' + env.str('REDIS_PORT') + '/0'
CELERY_ACCEPT_CONTENT = env.list('CELERY_ACCEPT_CONTENT')
CELERY_TASK_SERIALIZER = env.str('CELERY_TASK_SERIALIZER')
CELERY_RESULT_SERIALIZER = env.str('CELERY_RESULT_SERIALIZER')

