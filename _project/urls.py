from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include
from django.conf import settings

import debug_toolbar

urlpatterns = [
    path("admin/", admin.site.urls),
    path('', include('goods.urls')),
    path('accounts/', include('users.urls')),
    path('cart/', include('cart.urls')),
    path('__debug__/', include(debug_toolbar.urls))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

