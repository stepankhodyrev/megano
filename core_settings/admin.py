from django.contrib import admin
from .models import DeliverySettings


@admin.register(DeliverySettings)
class CoreSettingsAdmin(admin.ModelAdmin):
    list_display = ('id', 'min_amount_of_free_delivery', 'cost_of_paid_delivery')
