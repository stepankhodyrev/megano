from django.db import models
from django.utils.translation import gettext_lazy as _


class DeliverySettings(models.Model):
    min_amount_of_free_delivery = models.DecimalField(_('Min value of free delivery'), max_digits=10, decimal_places=2)
    cost_of_paid_delivery = models.DecimalField(_('Cost of paid delivery'), max_digits=10, decimal_places=2)
    priority = models.IntegerField(_('Priority index'), default=0)

    class Meta:
        verbose_name = _('Delivery settings')
        verbose_name_plural = _('Deliveries settings')

    def __str__(self):
        return f'{self.id}'
