console.clear();
let loadMoreContainer = document.querySelector(".container");
let items = Array.from(loadMoreContainer.querySelectorAll(".item-js"));
let loadMore = document.getElementById("loadMore");


let maxItems = 2;
let loadItems = 2;
let hiddenClass = "hidden";

items.forEach(function (item, index) {
  if (index > maxItems - 1) {
    item.classList.add(hiddenClass);
  }
});

loadMore.addEventListener("click", function () {
  [].forEach.call(document.querySelectorAll("." + hiddenClass), function (
    item,
    index
  ) {
    if (index < loadItems) {
      item.classList.remove(hiddenClass);
    }

    if (document.querySelectorAll("." + hiddenClass).length === 0) {
      loadMore.innerText = "This is the and. No more reviews";
      loadMore.classList.add('noContent')
    }
  });
});