const openModalButton = document.querySelector(".button.open");
const dialog = document.querySelector("dialog");
const closeModalButton = dialog.querySelector(".button.close");

openModalButton.addEventListener("click", () => {
  dialog.showModal();
});

closeModalButton.addEventListener("click", () => {
  dialog.close();
});