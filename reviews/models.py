from django.db import models
from django.utils.translation import gettext_lazy as _


class Review(models.Model):
    """Review model"""
    text = models.TextField(_('Text of review'), default='', max_length=400)
    user = models.ForeignKey('users.Account', on_delete=models.CASCADE, related_name='user_reviews',
                             verbose_name=_('User'))
    good = models.ForeignKey('goods.Good', on_delete=models.CASCADE, related_name='good_reviews',
                             verbose_name=_('Good'))
    date_of_review = models.DateTimeField(_('Date of review'), auto_now_add=True)

    class Meta:
        verbose_name = _('Review')
        verbose_name_plural = _('Reviews')

    def __str__(self):
        return f'{self.text}. {self.user.name}. {self.date_of_review}'
