from django import forms

from .models import Review


class ReviewForm(forms.ModelForm):
    """Form for reviews"""

    class Meta:
        model = Review
        fields = ('text', )

        labels = {
            'text': ''
        }

        widgets = {
            'text': forms.Textarea(
                attrs={"class": "form-textarea", "placeholder": "Review", "rows": 4})
        }
