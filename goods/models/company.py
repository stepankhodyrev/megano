from django.db import models
from django.utils.translation import gettext_lazy as _


class Company(models.Model):
    """Company model"""
    name = models.CharField(_('Company name'), default='', max_length=50)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _("Companies")

    def __str__(self):
        return self.name
