from django.db import models
from django.utils.translation import gettext_lazy as _


class Image(models.Model):
    """Good images model"""
    image = models.ImageField(_('Image file'), upload_to='goods/images',
                              max_length=255,
                              null=True,
                              blank=True)
    good = models.ForeignKey('goods.Good', on_delete=models.CASCADE, null=True, blank=True,
                             related_name='good_images', verbose_name=_('Image of good'))

    class Meta:
        verbose_name = _('Good image')
        verbose_name_plural = _('Good images')

    def __str__(self):
        return self.image.url
