from django.db import models
from django.utils.translation import gettext_lazy as _

from goods.models import Good


class Category(models.Model):
    """Goods category's model"""
    name = models.CharField(_('Name of category'), default='', max_length=50)
    sort_index = models.IntegerField(_('Sort index'), default=1)
    active_status = models.BooleanField(_('Active status'), default=False)
    icon = models.FileField(_('Logo of category'), upload_to='category/icons',
                            max_length=255,
                            null=True,
                            blank=True)
    image = models.ImageField(_('Image of category'), upload_to='category/images',
                              max_length=255,
                              null=True,
                              blank=True)
    subcategory = models.ManyToManyField('goods.Subcategory', blank=True,
                                         verbose_name=_('Subcategory'))

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.name

    def min_good_price(self):
        """Calculating min price of good in category"""
        goods = Good.objects.filter(category=self)
        min_price = None
        for i_good in goods:
            if not min_price or i_good.price < min_price:
                min_price = i_good.price
        return min_price


class Subcategory(models.Model):
    """Category subcategory's model"""
    name = models.CharField(_('Name of category'), default='', max_length=50)
    image = models.FileField(_('Logo of subcategory'), upload_to='subcategory/images',
                             max_length=255,
                             null=True,
                             blank=True)

    class Meta:
        verbose_name = _('Subcategory')
        verbose_name_plural = _("Subcategories")

    def __str__(self):
        return self.name
