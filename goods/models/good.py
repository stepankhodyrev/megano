from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from django.core.cache import cache


class Good(models.Model):
    """Good model"""
    name = models.CharField(_('Goods name'), default='', max_length=50)
    description = models.TextField(_('Goods description'), default='')
    sort_index = models.IntegerField(_('Sort index'), default=1)
    limited_edition = models.BooleanField(_('Limited edition flag'), default=False)
    date_of_adding = models.DateTimeField(_('Date of adding'), auto_now_add=True)
    price = models.DecimalField(_('Goods price'), max_digits=12, decimal_places=2)
    category = models.ManyToManyField('goods.Category', verbose_name='Category goods',
                                      related_name='goods_category')
    manufacturer = models.ForeignKey('goods.Company', verbose_name='Company goods',
                                     related_name='goods_company', on_delete=models.CASCADE)
    balance = models.IntegerField(_('The rest of good'), default=0)

    class Meta:
        verbose_name = _('Good')
        verbose_name_plural = _('Goods')

    def __str__(self):
        return f'{self.name} - {self.price}'

    def decrease_balance(self, value: int):
        if self.balance >= value:
            self.balance -= value
            self.save()
        else:
            raise ValueError(f'Not enough rest of {self.name}')


#  Deleting cache when edit good instance
@receiver(post_save, sender=Good)
def cost_post_save_receiver(sender, instance, created, *args, **kwargs):
    """Logging after saving instance"""
    if not created:
        cache.delete(instance.id)
