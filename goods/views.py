from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.views import generic
from django.core.cache import cache

from .business_logic import filtering_goods_by_orders_reviews, create_paginator_from_queryset
from .models import Good, Company, Category
from reviews.forms import ReviewForm
from .filters import GoodFilter


class GoodsMainPageView(generic.TemplateView):
    """Main page view"""
    template_name = 'goods/main_page.html'

    def get_context_data(self, **kwargs):
        context = super(GoodsMainPageView, self).get_context_data()
        context['top_products'] = Good.objects.select_related('manufacturer') \
                                      .prefetch_related('good_images').all().order_by('sort_index')[:8]
        context['limited_products'] = Good.objects.select_related('manufacturer') \
                                          .prefetch_related('good_images').filter(limited_edition=True)[:16]
        context['special_categories'] = Category.objects.only('name').order_by('-sort_index')[:3]
        return context


class GoodsListView(generic.ListView):
    """Catalog page view"""
    template_name = 'goods/catalog.html'
    model = Good
    context_object_name = 'goods'

    def get_queryset(self):
        queryset = super().get_queryset()
        ordering = self.request.GET.get('ordering', '')
        reviews = self.request.GET.get('reviews', '')
        get = self.request.GET
        filtered_goods = filtering_goods_by_orders_reviews(get_request=get, queryset=queryset, order=ordering,
                                                           reviews=reviews)
        goods_set = create_paginator_from_queryset(page=self.request.GET.get('page', ''), queryset=filtered_goods.qs)
        return goods_set

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(GoodsListView, self).get_context_data()
        context['manufacturers'] = Company.objects.all()
        context['form'] = GoodFilter(data=self.request.GET, queryset=Good.objects.all())
        return context


class GoodsListWithCategoryView(generic.TemplateView):
    """Catalog page view with filtering goods by category"""
    template_name = 'goods/catalog.html'

    def get_context_data(self, **kwargs):
        context = super(GoodsListWithCategoryView, self).get_context_data()
        context['manufacturers'] = Company.objects.all()
        category_name = kwargs['category']
        category = Category.objects.get(name__contains=category_name)
        queryset = Good.objects.filter(category=category)
        ordering = self.request.GET.get('ordering', '')
        reviews = self.request.GET.get('reviews', '')
        get = self.request.GET
        filtered_goods = filtering_goods_by_orders_reviews(get_request=get, queryset=queryset, order=ordering,
                                                           reviews=reviews, category=category_name)
        context['goods'] = create_paginator_from_queryset(page=self.request.GET.get('page', ''),
                                                          queryset=filtered_goods.qs)
        context['form'] = filtered_goods
        return context


class GoodsDetailView(generic.TemplateView):
    """Detail page of goods with caching"""
    template_name = 'goods/product_detail.html'

    def get_context_data(self, **kwargs):
        good_id = kwargs['pk']

        if cache.get(good_id):
            good = cache.get(good_id)
        else:
            try:
                good = Good.objects.prefetch_related('category', 'good_reviews').get(pk=good_id)
                cache.set(good_id, good)
            except Good.DoesNotExist:
                return HttpResponse('This good does not exist')

        context = {
            'good': good,
            'review_form': ReviewForm
        }

        return context

    def post(self, request, pk):
        good = get_object_or_404(Good, id=pk)
        review_form = ReviewForm(request.POST)

        if review_form.is_valid():
            review = review_form.save(commit=False)
            review.user = request.user
            review.good = good
            review.save()

            return HttpResponseRedirect(f'/goods/{good.id}/')
        return render(request, 'goods/main_page.html', {'good': good, 'form': review_form})
