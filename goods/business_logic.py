from django.core.paginator import Paginator
from django.db.models import Count

from goods.filters import GoodFilter


def filtering_goods_by_orders_reviews(get_request, queryset, category=None, order=None, reviews=None):
    """Function for filtering goods with FilterSet by django-filter"""

    if reviews:
        if reviews == 'more':
            queryset = queryset.annotate(cnt=Count('good_reviews')).order_by('-cnt')
        if reviews == 'less':
            queryset = queryset.annotate(cnt=Count('good_reviews')).order_by('cnt')
    if order:
        queryset = queryset.order_by(order)

    filtered_goods = GoodFilter(data=get_request, queryset=queryset, category=category)

    return filtered_goods


def create_paginator_from_queryset(page, queryset):
    """Pagination creator from queryset"""
    p = Paginator(queryset, 6)
    goods = p.get_page(page)
    return goods
