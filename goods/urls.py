from django.urls import path
from .views import GoodsMainPageView, GoodsListView, GoodsDetailView, GoodsListWithCategoryView

urlpatterns = [
    path('', GoodsMainPageView.as_view(), name='main-page'),
    path('goods/', GoodsListView.as_view(), name='catalog'),
    path('goods/<str:category>/', GoodsListWithCategoryView.as_view(), name='category-catalog'),
    path('goods/<str:category>/<int:pk>/', GoodsDetailView.as_view(), name='good-detail')
]
