import django_filters
from django.forms import CheckboxSelectMultiple

from .models import Good, Company


def manufacturer_filter(category):
    """Creating queryset for manufacturer's filter"""
    if not category:
        return Company.objects.prefetch_related('goods_company').filter(goods_company__isnull=False).distinct()
    return Company.objects.prefetch_related('goods_company').filter(goods_company__category__name=category).distinct()


class GoodFilter(django_filters.FilterSet):
    """Main filter on the catalog page"""
    price__gt = django_filters.NumberFilter(field_name='price', lookup_expr='gt')
    price__lt = django_filters.NumberFilter(field_name='price', lookup_expr='lt')
    manufacturer = django_filters.ModelMultipleChoiceFilter(field_name='manufacturer',
                                                            widget=CheckboxSelectMultiple())

    class Meta:
        model = Good
        fields = {
            'name': ['icontains'],
            'limited_edition': ['exact']
        }

    def __init__(self, category=None, *args, **kwargs):
        """Setting up queryset for Manufacturers and add classes for form fields"""
        super().__init__(*args, **kwargs)
        self.filters['manufacturer'].extra.update({
            'queryset': manufacturer_filter(category)
        })

        for i_filter in self.filters.values():
            i_filter.field.widget.attrs.update({'class': 'form-checkbox form-input form-input_full'})


class MainPageSearchFilter(django_filters.FilterSet):
    """Main search field in the header"""
    class Meta:
        model = Good
        fields = {
            'name': ['icontains']
        }
