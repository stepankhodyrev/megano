def categories(request):
    from goods.models import Category
    return {'categories': Category.objects.filter(active_status=True)\
                                          .prefetch_related('subcategory')\
                                          .order_by('sort_index')}


def search_filter(request):
    from goods.filters import MainPageSearchFilter
    return {'filter_form': MainPageSearchFilter()}
