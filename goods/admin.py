from django.contrib import admin
from .models import Good, Category, Company, Image, Subcategory


class ImagesInline(admin.TabularInline):
    model = Image


@admin.register(Good)
class GoodAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'date_of_adding', 'manufacturer')
    list_filter = ('manufacturer', 'category')
    search_fields = ('name', 'description')
    inlines = [
        ImagesInline
    ]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'sort_index', 'active_status')


@admin.register(Subcategory)
class SubcategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', )
