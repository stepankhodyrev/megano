from django.urls import path
from . import views


urlpatterns = [
    path('', views.cart_detail, name='cart_detail'),
    path('add/<int:product_id>/', views.cart_add, name='cart_add'),
    path('remove/<int:product_id>/', views.cart_remove, name='cart_remove'),
    path('remove_one_piece/<int:product_id>/<str:edit>', views.cart_edit_quantity, name='cart_edit_quantity'),
    path('first_step/', views.OrderStepOneView.as_view(), name='order_first_step'),
    path('second_step/', views.OrderStepTwoView.as_view(), name='order_second_step'),
    path('third_step/', views.OrderStepThreeView.as_view(), name='order_third_step'),
    path('fourth_step/', views.OrderStepFourView.as_view(), name='order_fourth_step'),
    path('checkout/', views.cart_checkout, name='cart_checkout'),
    path('pay/<int:purchase_id>/', views.PayPageView.as_view(), name='cart_pay'),
    path('pay/progress_payment/', views.ProgressPaymentPageView.as_view(), name='progress_payment'),
]