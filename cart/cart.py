from decimal import Decimal
from django.conf import settings
from django.db import transaction

from goods.models import Good
from purchases.models import Relationship, Purchase


class Cart(object):
    #  Изменить передачу объекта request
    def __init__(self, request):
        """
        Initializing the cart
        """
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            # save an empty cart in the session
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    def __iter__(self):
        """
        Iterating through the items in the cart and getting the products from the database
        """
        product_ids = self.cart.keys()
        # getting product objects and adding them to cart
        products = Good.objects.filter(id__in=product_ids)
        for product in products:
            self.cart[str(product.id)]['product'] = product

        for item in self.cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        """
        Counting all items in the cart
        """
        return sum(item['quantity'] for item in self.cart.values())

    def add(self, product, quantity=1, update_quantity=False):
        """
        Add a product to the cart or update its quantity
        """
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {'quantity': 0,
                                     'price': str(product.price)}
        if update_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        self.save()

    def save(self):
        # Update cart session
        self.session[settings.CART_SESSION_ID] = self.cart
        # Mark the session as "modified" to make sure it's saved
        self.session.modified = True

    def remove(self, product):
        """
        Removing an item from the cart
        """
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    def get_total_price(self):
        """
        Calculate the cost of items in the shopping cart
        """
        return sum(Decimal(item['price']) * item['quantity'] for item in
                   self.cart.values())

    def clear(self):
        # remove cart from session
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True

    def get_len(self):
        """
        Counting all items in the cart
        """
        return sum(item['quantity'] for item in self.cart.values())

    def edit_quantity(self, product, edit):
        """
        Update its quantity
        """
        product_id = str(product.id)
        product_rest = product.balance
        if edit == 'add':
            if self.cart[product_id]['quantity'] >= product_rest:
                raise ValueError('Max value of good already added to your cart')
            else:
                self.cart[product_id]['quantity'] += 1
        if edit == 'remove':
            if self.cart[product_id]['quantity'] == 1:
                self.remove(product)
            else:
                self.cart[product_id]['quantity'] -= 1
        self.save()

    def checkout(self, whole_purchase, user):
        price_of_goods = self.get_total_price()
        for item in self:
            product_count = item['product'].balance
            if product_count < item['quantity']:
                raise ValueError("You're trying to buy more product than its available")
        with transaction.atomic():
            for item in self:
                item['product'].decrease_balance(item['quantity'])
                purchase = Purchase.objects.create(
                    good=item['product'],
                    pieces=item['quantity'],
                    sum=item['total_price']
                )
                whole_purchase.purchase.add(purchase)
            whole_purchase.total_amount += price_of_goods
            whole_purchase.buyer = user
            whole_purchase.session = ''
            whole_purchase.save()

        return whole_purchase.id
