from _project.celery import app


@app.task
def validate_card_number(card):
    """Function-validator and errors generator"""
    if card[-1] != '0':
        return True
    else:
        return False
