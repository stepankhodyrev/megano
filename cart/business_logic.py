import random

from purchases.models import Delivery, Relationship
from core_settings.models import DeliverySettings
from .tasks import validate_card_number


def create_purchase(phone_number, email, name, session_key):
    """Func for creating Relationship object"""

    try:
        delivery_settings = DeliverySettings.objects.latest('-priority')
    except DeliverySettings.DoesNotExist:
        delivery_settings = DeliverySettings.objects \
            .create(min_amount_of_free_delivery=1000, cost_of_paid_delivery=200)

    whole_purchase, created = Relationship.objects.get_or_create(session=session_key, defaults={
        'delivery_settings': delivery_settings,
        'phone_number': phone_number,
        'email': email,
        'name': name
    })
    if not created:
        whole_purchase.phone_number = phone_number
        whole_purchase.email = email
        whole_purchase.name = name
        whole_purchase.delivery_settings = delivery_settings
        whole_purchase.save()


def update_total_amount(purchase, sign, new_amount):
    if sign == '+=':
        purchase.total_amount += new_amount
    elif sign == '-=':
        purchase.total_amount -= new_amount


def update_delivery_cost(purchase, new_delivery_cost):
    purchase.delivery_cost = new_delivery_cost


def purchase_without_delivery(delivery: Delivery, purchase: Relationship, cart_total: int) -> None:
    if delivery.name == 'Standard' \
            and cart_total <= purchase.delivery_settings.min_amount_of_free_delivery:
        update_total_amount(purchase=purchase,
                            sign='+=',
                            new_amount=purchase.delivery_settings.cost_of_paid_delivery)
        update_delivery_cost(purchase=purchase, new_delivery_cost=purchase.delivery_settings.cost_of_paid_delivery)
    else:
        update_total_amount(purchase=purchase,
                            sign='+=',
                            new_amount=delivery.price)
        update_delivery_cost(purchase=purchase, new_delivery_cost=delivery.price)


def purchase_with_delivery(delivery: Delivery, purchase: Relationship, cart_total: int) -> None:
    if delivery.name == 'Express' and purchase.delivery_cost == purchase.delivery_settings.cost_of_paid_delivery:
        update_delivery_cost(purchase=purchase, new_delivery_cost=delivery.price)
        purchase.total_amount -= purchase.delivery_settings.cost_of_paid_delivery + delivery.price
    if delivery.name == 'Express' \
            and purchase.delivery_method.name == 'Standard' \
            or delivery.name == 'Express' and purchase.total_amount == 0:
        purchase.total_amount += delivery.price
        update_delivery_cost(purchase=purchase, new_delivery_cost=delivery.price)
    elif delivery.name == 'Standard' and purchase.delivery_method.name == 'Express':
        purchase.total_amount -= purchase.delivery_method.price
        update_delivery_cost(purchase=purchase, new_delivery_cost=delivery.price)
    if delivery.name == 'Standard' \
            and cart_total <= purchase.delivery_settings.min_amount_of_free_delivery:
        purchase.total_amount += purchase.delivery_settings.cost_of_paid_delivery
        update_delivery_cost(purchase=purchase, new_delivery_cost=purchase.delivery_settings.cost_of_paid_delivery)


def add_delivery_method_to_purchase(cart, delivery, city, address, purchase):
    """Func for adding delivery method to existing Relationship object"""
    cart_total_amount = cart.get_total_price()
    purchase.city = city
    purchase.address = address
    if not purchase.delivery_method:
        purchase_without_delivery(delivery=delivery, purchase=purchase, cart_total=cart_total_amount)
    else:
        purchase_with_delivery(delivery=delivery, purchase=purchase, cart_total=cart_total_amount)
    purchase.delivery_method = delivery
    purchase.save()


def add_payment_method_to_purchase(payment_method, purchase):
    """Func for adding payment method to existing Relationship object"""
    purchase.payment_method = payment_method
    purchase.save()


def fake_payment(card, purchase):
    errors_choices = (
        'Bad luck',
        'Sorry, not your day',
        'Card number is fake',
        'You bet on zero'
    )
    valid_num = validate_card_number.delay(card)
    if valid_num:
        purchase.payment_status = True
    else:
        purchase.payment_status = False
        purchase.payment_error = random.choice(errors_choices)
    purchase.save()
