from django.contrib.auth import login
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic
from django.views.decorators.http import require_POST

from goods.models import Good, Image
from purchases.models import Relationship, Delivery, PaymentMethod
from .business_logic import create_purchase, add_delivery_method_to_purchase, \
    add_payment_method_to_purchase, fake_payment
from .cart import Cart
from .forms import CartAddProductForm
from purchases.forms import DeliveryForm, PaymentForm, ConfirmAccountPurchaseForm, CreateNewAccount


@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        good = get_object_or_404(Good, id=product_id)
        goods_rest = good.balance
        cd = form.cleaned_data
        if cd['quantity'] <= goods_rest:
            cart.add(product=good,
                     quantity=cd['quantity'],
                     update_quantity=cd['update'])
        else:
            return HttpResponse(content="You're trying to add more product than is available", status=200)
    return redirect('cart_detail')


def cart_remove(request, product_id):
    cart = Cart(request)
    good = get_object_or_404(Good, id=product_id)
    cart.remove(good)
    return redirect('cart_detail')


def cart_edit_quantity(request, product_id, edit):
    cart = Cart(request)
    good = get_object_or_404(Good, id=product_id)
    try:
        cart.edit_quantity(good, edit)
    except ValueError as error:
        return HttpResponse(content=error, status=200)
    return redirect('cart_detail')


def cart_detail(request):
    cart_data = dict()
    cart = Cart(request)
    for item in cart:
        cart_data[item['product'].id] = {
            'id': item['product'].id,
            'quantity': item['quantity'],
            'name': item['product'].name,
            'description': item['product'].description,
            'price': item['product'].price,
            'image_url': Image.objects.filter(good_id=item['product'].id).first(),
            'category': item['product'].category.first().name,
            'total_price': item['total_price']
        }
    total_price = cart.get_total_price()
    return render(request, 'cart/cart.html', {'cart_data': cart_data, 'total_price': total_price})


def cart_checkout(request):
    """Checkout cart and cleaning session"""
    cart = Cart(request)
    try:
        purchase = Relationship.objects.get(session=request.session.session_key)
        purchase_id = cart.checkout(whole_purchase=purchase, user=request.user)
        request.session[settings.CART_SESSION_ID] = {}
    except ValueError as err:
        #  Изменить статус ответа
        return HttpResponse(content=err, status=200)
    return redirect('cart_pay', purchase_id=purchase_id)


class OrderStepOneView(generic.TemplateView):
    """
    Checkout order step one view.
    Creating order instance
    """
    template_name = 'cart/order_first_step.html'

    def get_context_data(self, **kwargs):
        context = super(OrderStepOneView, self).get_context_data()
        if self.request.user.is_authenticated:
            context['signup_form'] = ConfirmAccountPurchaseForm(instance=self.request.user)
        else:
            context['signup_form'] = CreateNewAccount(),
            context['login_form'] = AuthenticationForm()
        return context

    def post(self, request):
        form = ConfirmAccountPurchaseForm(request.POST)
        session_key = request.session.session_key
        if request.user.is_authenticated:
            phone_number = form['phone_number'].value()
            email = form['email'].value()
            name = form['name'].value()
            create_purchase(phone_number=phone_number, email=email, name=name, session_key=session_key)
            return redirect('order_second_step')

        auth_form = CreateNewAccount(request.POST)
        if auth_form.is_valid():
            user = auth_form.save(commit=False)
            user.set_password(auth_form.cleaned_data["password"])
            user.save()
            login(request, user)
            phone_number = form['phone_number'].value()
            email = form['email'].value()
            name = form['name'].value()
            create_purchase(phone_number=phone_number, email=email, name=name, session_key=session_key)
            return redirect('order_second_step')

        return render(request, 'cart/order_first_step.html',
                      {'signup_form': auth_form, 'login_form': AuthenticationForm()})


class OrderStepTwoView(generic.TemplateView):
    """
    Checkout order step two view.
    Adding delivery method, city and address to existing order instance
    """
    template_name = 'cart/order_second_step.html'

    def get_context_data(self, **kwargs):
        context = {
            'delivery_form': DeliveryForm()
        }
        return context

    def post(self, request):
        form = DeliveryForm(request.POST)
        purchase = Relationship.objects.get(session=request.session.session_key)
        if not purchase:
            return HttpResponse(content="Something went wrong. Try to checkout again from first step", status=200)
        cart = Cart(request)
        delivery = get_object_or_404(Delivery, id=form['delivery_method'].value())
        city = form['city'].value()
        address = form['address'].value()
        add_delivery_method_to_purchase(cart=cart, delivery=delivery,
                                        city=city, address=address, purchase=purchase)
        return redirect('order_third_step')


class OrderStepThreeView(generic.TemplateView):
    """
    Checkout order step three view.
    Adding payment method to existing order
    """
    template_name = 'cart/order_third_step.html'

    def get_context_data(self, **kwargs):
        context = {
            'payment_form': PaymentForm
        }
        return context

    def post(self, request):
        form = PaymentForm(request.POST)
        purchase = Relationship.objects.get(session=request.session.session_key)
        if not purchase:
            return HttpResponse(content="Something went wrong. Try to checkout again from first step", status=200)
        payment_method = get_object_or_404(PaymentMethod, id=form['payment_method'].value())
        add_payment_method_to_purchase(payment_method=payment_method, purchase=purchase)
        return redirect('order_fourth_step')


class OrderStepFourView(generic.TemplateView):
    """
    Checkout order step four view.
    Confirming order page
    """
    template_name = 'cart/order_fourth_step.html'

    def get_context_data(self, **kwargs):
        cart = Cart(self.request)
        whole_purchase = Relationship.objects.get(session=self.request.session.session_key)
        context = {
            'purchase': whole_purchase,
            'cart': cart,
            'total_amount': whole_purchase.delivery_cost + cart.get_total_price()
        }
        return context


class PayPageView(generic.View):
    """
    Paid page view with two different templates.
    Templates differ depending on the payment method
    """

    def get(self, request, purchase_id):
        order = Relationship.objects.get(id=purchase_id)
        if order.payment_method.id == 1:
            return render(request, 'cart/pay_page.html', {})
        if order.payment_method.id == 2:
            return render(request, 'cart/pay_page_from_random.html', {})

    def post(self, request, purchase_id):
        card_number = request.POST.get('numero1')
        purchase = Relationship.objects.get(id=purchase_id)
        fake_payment(card_number, purchase)
        return render(request, 'cart/progress_payment.html', {})


class ProgressPaymentPageView(generic.View):
    """Page with progress payment animation"""

    def get(self, request):
        return render(request, 'cart/progress_payment.html', {})
