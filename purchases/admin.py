from django.contrib import admin
from .models import Purchase, Relationship, Delivery, PaymentMethod


@admin.register(Purchase)
class PurchaseAdmin(admin.ModelAdmin):
    list_display = ('id', 'pieces', 'sum')


@admin.register(Relationship)
class RelationshipAdmin(admin.ModelAdmin):
    list_display = ('buyer', 'phone_number', 'date_of_purchase', 'total_amount', 'email', 'payment_status')
    readonly_fields = ('buyer', 'total_amount', 'session', 'payment_status', 'payment_error')
    list_filter = ('date_of_purchase', 'payment_status')
    search_fields = ('phone_number', 'email')


@admin.register(Delivery)
class DeliveryAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')


@admin.register(PaymentMethod)
class PaymentMethodAdmin(admin.ModelAdmin):
    list_display = ('name', )
