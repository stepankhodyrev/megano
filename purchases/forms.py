from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.html import format_html

from .models import Delivery, PaymentMethod, Relationship
from users.models import Account


class DeliveryForm(forms.Form):
    """Form for sending delivery method with address information"""
    delivery_method = forms.ModelChoiceField(queryset=Delivery.objects.all(),
                                             widget=forms.RadioSelect())
    city = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-input'
    }))
    address = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'form-input',
        'rows': 2
    }))


class PaymentForm(forms.Form):
    """Form for sending payment method information"""
    payment_method = forms.ModelChoiceField(queryset=PaymentMethod.objects.all(),
                                            widget=forms.RadioSelect())


class ConfirmAccountPurchaseForm(forms.ModelForm):
    """Form for sending personal information from checkout"""
    class Meta:
        model = Account
        fields = ('name', 'phone_number', 'email')

        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'form-input'
            }),
            'name': forms.TextInput(attrs={
                'class': 'form-input',
            }),
            'phone_number': forms.TextInput(attrs={
                'class': 'form-input',
                'type': "tel",
                'maxlength': '18',
                'data-phone-input': "",
            })
        }


class CreateNewAccount(forms.ModelForm):
    """Form for creating new account from checkout"""
    password = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(attrs={'class': 'form-input'}),
    )
    confirm_password = forms.CharField(
        label="Confirm password",
        widget=forms.PasswordInput(attrs={'class': 'form-input'}),
    )

    class Meta:
        model = Account
        fields = ('name', 'phone_number', 'email', 'password')

        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'form-input'
            }),
            'name': forms.TextInput(attrs={
                'class': 'form-input',
            }),
            'phone_number': forms.TextInput(attrs={
                'class': 'form-input',
                'type': "tel",
                'maxlength': '18',
                'data-phone-input': "",
            })
        }

    def clean_email(self):
        email = self.cleaned_data['email']
        if Account.objects.filter(email=email).exists():
            raise ValidationError(format_html('<div class="">'
                                              'Account with this email already exist '
                                              '<button class="ControlPanel-title button default open black">Login'
                                              '</button>'
                                              '</div>'))
        return email

    def clean(self):
        cleaned_data = super(CreateNewAccount, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            raise ValidationError(
                "Password and Confirm password does not match"
            )

