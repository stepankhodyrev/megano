from django.db import models
from django.utils.translation import gettext_lazy as _


class Relationship(models.Model):
    """Purchases to session relationship"""
    buyer = models.ForeignKey('users.Account', null=True, on_delete=models.CASCADE,
                              related_name='buyer_relationship', verbose_name=_('Buyer'))
    session = models.CharField(_('Session'), default='', max_length=100)
    date_of_purchase = models.DateTimeField(_('Date of purchase'), auto_now_add=True)
    purchase = models.ManyToManyField('purchases.Purchase', verbose_name=_('Purchase'))
    total_amount = models.DecimalField(_('Total'), default=0, decimal_places=2, max_digits=20)
    delivery_method = models.ForeignKey('purchases.Delivery', on_delete=models.CASCADE,
                                        related_name='delivery_method', null=True, blank=True)
    delivery_settings = models.ForeignKey('core_settings.DeliverySettings', on_delete=models.CASCADE,
                                          related_name='delivery_settings', null=True, blank=True)
    delivery_cost = models.DecimalField(_('Delivery cost'), default=0, decimal_places=2, max_digits=20)
    payment_method = models.ForeignKey('purchases.PaymentMethod', on_delete=models.CASCADE,
                                       related_name='payment_method', null=True, blank=True)
    city = models.CharField(_('Delivery city'), max_length=30, default='')
    address = models.TextField(_('Delivery address'), max_length=100, default='')
    name = models.CharField(_('Name'), max_length=30, default='')
    phone_number = models.CharField(_('Phone number'), max_length=25, default='')
    email = models.EmailField(_('Email'), max_length=60, blank=True, null=True)
    payment_status = models.BooleanField(_('Payment status'), default=False)
    payment_error = models.CharField(_('Payment error'), default='', max_length=100)

    class Meta:
        verbose_name = _('Relationship')
        verbose_name_plural = _('Relationships')

    def __str__(self):
        return f'{self.buyer}. {self.date_of_purchase}'
