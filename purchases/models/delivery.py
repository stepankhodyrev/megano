from django.db import models
from django.utils.translation import gettext_lazy as _


class Delivery(models.Model):
    """Delivery method model"""
    name = models.CharField(_('Name of delivery method'), max_length=20, default='')
    price = models.DecimalField(_('Price of delivery'), max_digits=10, decimal_places=2)

    class Meta:
        verbose_name = _('Delivery method')
        verbose_name_plural = _('Delivery methods')

    def __str__(self):
        return self.name
