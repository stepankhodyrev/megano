from django.db import models
from django.utils.translation import gettext_lazy as _


class Purchase(models.Model):
    """Purchases model"""
    good = models.ForeignKey('goods.Good', null=True, on_delete=models.CASCADE,
                             related_name='good_purchases', verbose_name=_('Good'))
    pieces = models.IntegerField(_('Pieces'), default=0)
    sum = models.DecimalField(_('Sum'), default=0, decimal_places=2, max_digits=20)

    class Meta:
        verbose_name = _('Purchase')
        verbose_name_plural = _('Purchases')

    def __str__(self):
        return f'{self.good}.{self.pieces}pc. {self.sum}'
