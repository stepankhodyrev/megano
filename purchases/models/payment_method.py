from django.db import models
from django.utils.translation import gettext_lazy as _


class PaymentMethod(models.Model):
    """Payment method model"""
    name = models.CharField(_('Name of payment method'), default='', max_length=40)

    class Meta:
        verbose_name = _('Payment method')
        verbose_name_plural = _('Payment methods')

    def __str__(self):
        return self.name
