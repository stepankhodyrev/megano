from users.views import ResetPasswordView
from django.urls import path
from django.contrib.auth import views as auth_views

from .views import LogInView, LogOutView, AccountPageView, \
                   EditProfileView, SignUpView, HistoryOrderView, OrderDetailView
from .forms import UserLoginForm


urlpatterns = [
    path('login/', LogInView.as_view(authentication_form=UserLoginForm), name='login'),
    path('logout/', LogOutView.as_view(), name='logout'),
    path('', AccountPageView.as_view(), name='account'),
    path('signup/', SignUpView.as_view(), name='registration'),
    path('<int:pk>/update', EditProfileView.as_view(), name='edit_profile'),
    path('history_order', HistoryOrderView.as_view(), name='history_order'),
    path('history_order/<int:pk>', OrderDetailView.as_view(), name='order_detail'),

    #  Reset password
    path('password_reset/', auth_views.PasswordResetView.as_view(
        template_name='users/password_reset.html'), name='reset_password'),
    path('reset_password_sent', auth_views.PasswordResetDoneView.as_view(
        template_name='users/password_reset_done.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
        template_name='users/password_reset_confirm.html'), name='password_reset_confirm'),
    path('reset_password_complete', auth_views.PasswordResetCompleteView.as_view(
        template_name='users/password_reset_complete.html'), name='password_reset_complete'),
]
