import re

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from django.db import models

from utils.uploaders import image_uploader


class MyUserManager(BaseUserManager):
    """Custom user manager"""
    def create_user(self, email, username, password=None):
        """
        Creates and saves a User with the given email, username and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            username=username,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)
        return user


def get_profile_image_path(self):
    return f'profile_images/{self.pk}/{"profile_image.jpg"}'


def get_default_profile_image():
    return 'images/basic-avatar.jpg'


class Account(AbstractBaseUser):
    """Users account models"""
    email = models.EmailField(_('Email'), max_length=60, unique=True)
    username = models.CharField(_('Username'), max_length=30, unique=True, null=True, blank=True)
    name = models.CharField(_('Name'), max_length=30)
    date_joined = models.DateTimeField(_('Date of registration'), auto_now_add=True)
    last_login = models.DateTimeField(_('Last log in'), auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    profile_image = models.ImageField(upload_to=image_uploader,
                                      max_length=255,
                                      null=True,
                                      blank=True)
    hide_email = models.BooleanField(default=True)
    date_of_birth = models.DateField(_('Date of birth'), auto_now=False, null=True)
    phone_number = models.CharField(_('Phone number'), max_length=25, default='', unique=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return str(self.username)

    def get_profile_image_filename(self):
        return str(self.profile_image)[str(self.profile_image).index(f'profile_images/{self.pk}/'):]

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    def is_exist(self):
        if Account.objects.filter(email=self.email):
            return True
        return False

    @property
    def get_account_image(self):
        if self.profile_image:
            return self.profile_image.url
        return '/static/images/user_icon.svg'


#  Editing phone number after save
@receiver(post_save, sender=Account)
def cost_post_save_receiver(sender, instance, created, *args, **kwargs):
    """Logging after saving instance"""
    if instance.phone_number:
        phone = instance.phone_number
        if phone.startswith('8'):
            instance.phone_number = re.sub("\D", "", phone[1:])
            instance.save()
        elif phone.startswith('+7'):
            instance.phone_number = re.sub("\D", "", phone[2:])
            instance.save()
