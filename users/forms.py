from django import forms
from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm

from .models import Account


class UserLoginForm(AuthenticationForm):
    """Login form"""
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

    username = UsernameField(label='', widget=forms.TextInput(
        attrs={'class': 'form-control login', 'placeholder': 'Email'}))
    password = forms.CharField(label='', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control login',
            'placeholder': 'Password',
        }
    ))


class UpdateProfileForm(forms.ModelForm):
    """Form for updating personal information"""
    phone_number = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-input',
        'type': "tel",
        'maxlength': '18',
        'data-phone-input': "",
    }))

    class Meta:
        model = Account
        fields = ('profile_image', 'name', 'phone_number', 'email',)

        widgets = {
            'email': forms.EmailInput(attrs={
                'class': 'form-input',
            }),
            'name': forms.TextInput(attrs={
                'class': 'form-input',
            }),
            'profile_image': forms.FileInput(attrs={
                'class': 'form-input',
            })
        }


class SignUpForm(UserCreationForm):
    """Sign up form"""

    class Meta:
        model = Account
        fields = ('email', 'username', 'name', 'phone_number', 'password1', 'password2',)
