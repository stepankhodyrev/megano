from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, UpdateView, CreateView, DetailView
from django.contrib.auth.views import PasswordResetView


from .forms import UpdateProfileForm, SignUpForm
from purchases.models import Relationship
from .models import Account


class LogInView(LoginView):
    """Login page view"""
    template_name = 'users/login.html'

    def form_valid(self, form):
        super().form_valid(form)
        if self.request.META.get('HTTP_REFERER').endswith('login/'):
            return HttpResponseRedirect(self.get_success_url())
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class LogOutView(LogoutView):
    """Logout page view"""
    template_name = 'users/logout.html'


class AccountPageView(LoginRequiredMixin, TemplateView):
    """Personal account page"""
    login_url = 'login/'
    template_name = 'users/account.html'

    def get_context_data(self, **kwargs):
        context = super(AccountPageView, self).get_context_data()
        context['last_order'] = Relationship.objects.filter(buyer_id=self.request.user.id).latest('date_of_purchase')
        return context


class EditProfileView(LoginRequiredMixin, UpdateView):
    """Edit personal info page"""
    login_url = '/login/'
    model = Account
    template_name = 'users/edit_profile.html'
    form_class = UpdateProfileForm
    success_url = reverse_lazy('account')


class SignUpView(CreateView):
    """Sign up page view"""
    model = Account
    template_name = 'users/registration.html'
    form_class = SignUpForm
    success_url = 'accounts/'


class ResetPasswordView(SuccessMessageMixin, PasswordResetView):
    """Reset password page view"""
    template_name = 'users/password_reset.html'
    email_template_name = 'users/password_reset_email.html'
    subject_template_name = 'users/password_reset_subject'
    success_message = "We've emailed you instructions for setting your password, " \
                      "if an account exists with the email you entered. You should receive them shortly." \
                      " If you don't receive an email, " \
                      "please make sure you've entered the address you registered with, and check your spam folder."
    success_url = reverse_lazy('catalog')


class HistoryOrderView(TemplateView):
    """History order page view"""
    template_name = 'users/history_order.html'

    def get_context_data(self, **kwargs):
        context = dict()
        context['purchases'] = Relationship.objects.select_related('buyer', 'delivery_method', 'payment_method')\
            .only('buyer', 'date_of_purchase', 'total_amount', 'delivery_method__name', 'payment_method__name',
                  'payment_status', 'payment_error')\
            .filter(buyer_id=self.request.user.id).order_by('-date_of_purchase')
        return context


class OrderDetailView(DetailView):
    """Order detail page view"""
    model = Relationship
    template_name = 'users/order_detail.html'
    context_object_name = 'order'




