from django.contrib import admin
from .models import Account


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('email', 'phone_number', 'username', 'date_of_birth',
                    'last_login', 'date_joined', 'is_admin', 'is_staff')
    search_fields = ('email', 'username', 'phone_number', 'date_of_birth')
    readonly_fields = ('id', 'date_joined', 'last_login',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()
